<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
		return view('form');
	}
	public function welcome(Request $request){
		$first = $request["fn"];
		$last = $request["ln"];
		return view('last', ["first" => $first, "last" => $last]);
	}
}
