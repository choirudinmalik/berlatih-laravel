<!DOCTYPE html>
<html>
    <head>
		<title>Form</title>
    </head>
    <body>
		<h1> Buat Account Baru! </h1>
		<h3> Sign Up Form </h3>
		<form action="/welcome" method="POST">
			@csrf
			<label for="fn"> First name: </label>
			<br><br>
			<input type="text" name="fn">
			<br><br>
			<label for="ln"> Last name: </label>
			<br><br>
			<input type="text" name="ln">
			<br><br>
			<label> Gender: </label>
			<br><br>
			<input type="radio" name="Gender" value="0"> Male <br>
			<input type="radio" name="Gender" value="1"> Female <br>
			<input type="radio" name="Gender" value="2"> Other
			<br><br>
			<label> Nationality: </label>
			<br><br>
			<select>
				<option value="0"> Indonesian </option>
				<option value="1"> Malaysian </option>
				<option value="2"> Singaporean </option>
				<option value="3"> Australian </option>
			</select>
			<br><br>
			<label> Language Spoken: </label>
			<br><br>
			<input type="checkbox" name="Lang" value="0"> Bahasa Indonesia <br>
			<input type="checkbox" name="Lang" value="1"> English <br>
			<input type="checkbox" name="Lang" value="2"> Other
			<br><br>
			<label for="bio"> Bio: </label>
			<br><br>
			<textarea cols="50" rows="20" id="bio"> </textarea>
			<br>
			<input type="submit" value="Sign Up">
		</form>
    </body>
</html>